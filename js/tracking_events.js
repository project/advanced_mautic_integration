/**
 * @file
 * Advanced Mautic Integration behaviors.
 */

/* global mt */

((Drupal) => {
  /**
   * This array contains all pageviews that have been sent to Mautic.
   * This is used to prevent duplicate pageviews in one request.
   */
  Drupal.mt_pageviews = [];

  /**
   * This behavior sends tracking events to Mautic.
   */
  Drupal.behaviors.advancedMauticIntegrationTrackingEvents = {
    attach(context, settings) {
      /**
       * Send a pageview event to Mautic.
       *
       * You may use this function anywhere in your code.
       * Just include the advanced_mautic_integration/tracking_events library.
       *
       * @param {object} extraParams
       *  An object containing additional parameters to send to Mautic.
       *  The params can be: page_url, referrer, language, and page_title.
       *  See: https://docs.mautic.org/en/contacts/manage-contacts/contact-monitoring#page-information
       *
       * @return {void}
       */
      Drupal.mt_send = (extraParams = {}) => {
        const url = extraParams.page_url || window.location.href;
        if (Drupal.mt_pageviews.indexOf(url) === -1) {
          const params =
            settings.advancedMauticIntegration.trackDefaultParameters;
          Object.assign(params, extraParams);
          if (typeof mt === 'function') {
            mt('send', 'pageview', params);
          }
          Drupal.mt_pageviews.push(url);
        }
      };

      const isInternal = (url) => {
        const isInternalRegex = new RegExp(
          `^(https?)://${window.location.host}`,
          'i',
        );
        return isInternalRegex.test(url);
      };

      const isDownload = (url) => {
        const isDownloadRegex = new RegExp(
          `\\.(${settings.advancedMauticIntegration.trackDownloadExtensions})([?#].*)?$`,
          'i',
        );
        return isDownloadRegex.test(url);
      };

      const isMailto = (url) => {
        const isInternalRegex = /^mailto:/i;
        return isInternalRegex.test(url);
      };

      const isTel = (url) => {
        const isInternalRegex = /^tel:/i;
        return isInternalRegex.test(url);
      };

      const isProperExternal = (url) => {
        const isProperExternalRegex = /^\w+:\/\//i;
        return isProperExternalRegex.test(url);
      };

      // Track ordinary pageviews.
      if (settings.advancedMauticIntegration.trackPageviews) {
        Drupal.mt_send();
      }

      // Bind events to <a> tags.
      once('bind-mautic-tracking', 'a', context).forEach((el) => {
        const trackLink = (event) => {
          const url = event.target.href;

          if (isInternal(url)) {
            if (isDownload(url)) {
              if (settings.advancedMauticIntegration.trackDownload) {
                Drupal.mt_send({
                  page_url: url,
                  page_title: `Download ${url}`,
                  referrer: window.location.href,
                });
              }
            }
          } else if (isMailto(url)) {
            if (settings.advancedMauticIntegration.trackMailto) {
              Drupal.mt_send({
                page_url: url,
                page_title: `Mail to: ${url.substr(7)}`,
                referrer: window.location.href,
              });
            }
          } else if (isTel(url)) {
            if (settings.advancedMauticIntegration.trackTel) {
              Drupal.mt_send({
                page_url: url,
                page_title: `Phone: ${url.substr(5)}`,
                referrer: window.location.href,
              });
            }
          } else if (
            settings.advancedMauticIntegration.trackOutbound &&
            isProperExternal(url)
          ) {
            Drupal.mt_send({
              page_url: url,
              page_title: `External link: ${url}`,
              referrer: window.location.href,
            });
          }
        };

        el.addEventListener('mousedown', trackLink);
        el.addEventListener('keyup', trackLink);
        el.addEventListener('touchstart', trackLink);
      });
    },
  };
})(Drupal);
