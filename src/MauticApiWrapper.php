<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Mautic\Api\Api;
use Mautic\Auth\ApiAuth;
use Mautic\Auth\AuthInterface;
use Mautic\MauticApi;

/**
 * The wrapper for the Mautic API library.
 */
final class MauticApiWrapper implements MauticApiWrapperInterface {

  /**
   * The API authentication interface.
   *
   * @var \Mautic\Auth\AuthInterface|null
   */
  private ?AuthInterface $apiAuth = NULL;

  /**
   * Constructs a MauticApiWrapper object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getApi($api_context): Api {
    $config = $this->configFactory->get('advanced_mautic_integration.settings');

    if (is_null($this->apiAuth)) {
      $config = $this->configFactory->get('advanced_mautic_integration.settings');

      $settings = [
        'userName' => $config->get('api.user'),
        'password' => $config->get('api.password'),
      ];

      $initAuth = new ApiAuth();
      $this->apiAuth = $initAuth->newAuth($settings, 'BasicAuth');
    }

    $apiUrl = $config->get('api.url');
    $api = new MauticApi();
    return $api->newApi($api_context, $this->apiAuth, $apiUrl);
  }

}
