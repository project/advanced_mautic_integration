<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration;

use Drupal\Core\Render\BubbleableMetadata;

/**
 * An interface for Mautic JS generator.
 */
interface MauticScriptInterface {

  /**
   * Get the Mautic JS snippet.
   *
   * @param \Drupal\Core\Render\BubbleableMetadata|null $bubbleable_metadata
   *   The bubbleable metadata.
   *
   * @return string
   *   The JS snippet.
   */
  public function getScript(BubbleableMetadata $bubbleable_metadata = NULL): string;

  /**
   * Get the tracking settings to pass as drupalSettings.
   *
   * @param \Drupal\Core\Render\BubbleableMetadata|null $bubbleable_metadata
   *   The bubbleable metadata.
   *
   * @return array
   *   The tracking settings.
   */
  public function getTrackingSettings(BubbleableMetadata $bubbleable_metadata = NULL): array;

}
