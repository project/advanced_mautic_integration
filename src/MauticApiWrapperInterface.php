<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration;

use Mautic\Api\Api;

/**
 * The interface for the Mautic API wrapper.
 */
interface MauticApiWrapperInterface {

  /**
   * Get one of the Mautic APIs.
   *
   * @param string $api_context
   *   The API context.
   *
   * @return \Mautic\Api\Api
   *   The API object.
   */
  public function getApi($api_context): Api;

}
