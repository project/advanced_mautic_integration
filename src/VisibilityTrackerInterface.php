<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration;

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Interface for the VisibilityTracker.
 */
interface VisibilityTrackerInterface {

  /**
   * Check the Mautic script visibility.
   *
   * @return bool
   *   TRUE if the script is visible, FALSE otherwise.
   */
  public function isVisible(BubbleableMetadata $bubbleable_metadata = NULL): bool;

}
