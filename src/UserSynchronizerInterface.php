<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration;

use Drupal\user\UserInterface;

/**
 * Defines an interface for contact synchronizer.
 */
interface UserSynchronizerInterface {

  /**
   * Synchronizes the Drupal user with Mautic lead.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to synchronize.
   */
  public function push(UserInterface $user): void;

  /**
   * Get the lead ID of Drupal user in Mautic.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to get the lead ID for.
   *
   * @return string|null
   *   The current lead ID, or null if not available.
   */
  public function getLeadIdForUser(UserInterface $user): ?string;

  /**
   * Convert the Drupal user to Mautic lead.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user to convert.
   *
   * @return array
   *   The Mautic lead data.
   */
  public function convertUserToLead(UserInterface $user): array;

  /**
   * Get the lead by email.
   *
   * @param string $parameter
   *   The parameter, like mail address.
   *
   * @return array|null
   *   The lead data, or null if not found.
   */
  public function getLeadByParameter($parameter): ?array;

}
