<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration;

use Drupal\Component\Plugin\Exception\ContextException;
use Drupal\Component\Plugin\Exception\MissingValueContextException;
use Drupal\Core\Condition\ConditionAccessResolverTrait;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Context\ContextHandlerInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Plugin\ContextAwarePluginInterface;
use Drupal\Core\Plugin\FilteredPluginManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;

/**
 * Defines the VisibilityTracker class.
 */
class VisibilityTracker implements VisibilityTrackerInterface {

  use ConditionAccessResolverTrait;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Plugin\FilteredPluginManagerInterface $conditionManager
   *   The condition plugin manager service.
   * @param \Drupal\Core\Plugin\Context\ContextHandlerInterface $contextHandler
   *   The ContextHandler for applying contexts to conditions properly.
   * @param \Drupal\Core\Plugin\Context\ContextRepositoryInterface $contextRepository
   *   The lazy context repository service.
   */
  public function __construct(
    // @todo add to service definition
    private readonly ConfigFactoryInterface $configFactory,
    private readonly FilteredPluginManagerInterface $conditionManager,
    private readonly ContextHandlerInterface $contextHandler,
    private readonly ContextRepositoryInterface $contextRepository,
  ) {}

  /**
   * Check the Mautic script visibility.
   */
  public function isVisible(BubbleableMetadata $bubbleable_metadata = NULL): bool {
    $config = $this->configFactory->get('advanced_mautic_integration.settings');
    $bubbleable_metadata->addCacheableDependency($config);

    // Do not continue if the tracking URL is not set.
    if (!$config->get('track.url')) {
      return FALSE;
    }

    $visibility_config = $config->get('visibility');

    $conditions = [];
    $missing_context = FALSE;
    $missing_value = FALSE;

    $visibility_collection = new ConditionPluginCollection($this->conditionManager, $visibility_config);
    foreach ($visibility_collection as $condition_id => $condition) {
      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      if ($condition instanceof ContextAwarePluginInterface) {
        try {
          $contexts = $this->contextRepository->getRuntimeContexts(array_values($condition->getContextMapping()));
          $this->contextHandler->applyContextMapping($condition, $contexts);
        }
        catch (MissingValueContextException $e) {
          $missing_value = TRUE;
        }
        catch (ContextException $e) {
          $missing_context = TRUE;
        }

        $bubbleable_metadata->addCacheableDependency($condition);
      }
      $conditions[$condition_id] = $condition;
    }

    if ($missing_context) {
      // If any context is missing then we might be missing cacheable
      // metadata, and don't know based on what conditions the block is
      // accessible or not. Make sure the result cannot be cached.
      // @todo disable cache here
      $bubbleable_metadata->setCacheMaxAge(0);
      return FALSE;
    }
    elseif ($missing_value) {
      // The contexts exist but have no value. Deny access without
      // disabling caching. For example the node type condition will have a
      // missing context on any non-node route like the frontpage.
      return FALSE;
    }
    elseif ($this->resolveConditions($conditions, 'and') === FALSE) {
      // Conditions failed.
      return FALSE;
    }

    return TRUE;
  }

}
