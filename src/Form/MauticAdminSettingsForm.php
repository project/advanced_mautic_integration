<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Condition\ConditionInterface;
use Drupal\Core\Condition\ConditionPluginCollection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Plugin\FilteredPluginManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure advanced_mautic_integration settings for this site.
 */
class MauticAdminSettingsForm extends ConfigFormBase {

  /**
   * The constructor method.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The manages modules.
   * @param \Drupal\Core\Plugin\FilteredPluginManagerInterface $conditionManager
   *   The ConditionManager for building the visibility UI.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    protected AccountInterface $currentUser,
    protected ModuleHandlerInterface $moduleHandler,
    protected FilteredPluginManagerInterface $conditionManager,
    protected LanguageManagerInterface $languageManager,
  ) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('module_handler'),
      $container->get('plugin.manager.condition'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'advanced_mautic_integration_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['advanced_mautic_integration.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('advanced_mautic_integration.settings');

    $form['javascript'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mautic JS snippet settings'),
    ];
    $form['javascript']['track_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mautic base URL'),
      '#default_value' => $config->get('track.url'),
      '#description' => $this->t('The base URL of your Mautic installation. Example: https://mautic.example.com. This URL will be used to load the Mautic JavaScript library. Remember to set the CORS headers on your Mautic installation to include this website. <br><strong>Clear this field to disable Mautic JS</strong>.'),
      '#required' => FALSE,
    ];
    $form['javascript']['track_pageviews'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track pageviews'),
      '#default_value' => $config->get('track.pageviews') ?? TRUE,
    ];
    $form['javascript']['track_outbound'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track clicks on outbound links'),
      '#default_value' => $config->get('track.outbound'),
    ];
    $form['javascript']['track_mailto'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track clicks on mailto (email) links'),
      '#default_value' => $config->get('track.mailto'),
    ];
    $form['javascript']['track_tel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track clicks on tel (telephone number) links'),
      '#default_value' => $config->get('track.tel'),
    ];
    $form['javascript']['track_files'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Track downloads (clicks on file links) for the following extensions'),
      '#default_value' => $config->get('track.files'),
    ];
    $form['javascript']['track_files_extensions'] = [
      '#title' => $this->t('List of download file extensions'),
      '#title_display' => 'invisible',
      '#type' => 'textfield',
      '#default_value' => $config->get('track.files_extensions'),
      '#description' => $this->t('A file extension list separated by the | character that will be tracked as download when clicked. Regular expressions are supported. For example: "pdf|doc|zip"'),
      '#maxlength' => 500,
      '#states' => [
        'enabled' => [
          ':input[name="track_files"]' => ['checked' => TRUE],
        ],
        // Note: Form required marker is not visible as title is invisible.
        'required' => [
          ':input[name="track_files"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['javascript']['track_default_parameters'] = [
      '#title' => $this->t('Default parameters sent with every event'),
      '#type' => 'textarea',
      '#default_value' => $config->get('track.default_parameters') ?: "{\n\"email\": \"[current-user:mail]\"\n}",
      '#description' => $this->t('A JSON object containing the default parameters sent with every event.<br>For example: <code>{"email": "[current-user:mail]"}</code>.<br>Empty values will be ignored.<br>Please note that you must set all Mautic fields defined here to "Publicly updatable".'),
      '#token' => TRUE,
    ];
    $form['javascript']['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [],
      '#global_types' => TRUE,
      '#weight' => 90,
    ];

    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Mautic API settings'),
    ];
    $form['api']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mautic API URL'),
      '#default_value' => $config->get('api.url'),
      '#description' => $this->t('The API URL of your Mautic installation. Example: https://mautic.example.com/api. This URL will be used to contact the Mautic API. <br><strong>Clear this field to disable Mautic API</strong>.'),
      '#required' => FALSE,
    ];
    $form['api']['api_user'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mautic API user'),
      '#default_value' => $config->get('api.user'),
      '#description' => $this->t('The API user of your Mautic installation. This user must have access to the API.'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="api_url"]' => ['filled' => TRUE],
        ],
      ],
    ];
    $form['api']['api_password'] = [
      '#type' => 'password',
      '#title' => $this->t('Mautic API password'),
      '#default_value' => $config->get('api.password'),
      '#description' => $this->t('The API password of your Mautic installation. Leave empty to keep the current password.'),
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="api_url"]' => ['filled' => TRUE],
        ],
      ],
    ];
    $form['api']['api_synchronize_user'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Synchronize Drupal user with Mautic lead'),
      '#default_value' => $config->get('api.synchronize_user'),
      '#description' => $this->t('When enabled, Mautic leads will be updated via API when Drupal user is updated.<br>This will work properly only if the <code>mtc_id</code> cookie is visible for Drupal (when https is used on both sides, and CORS is set up).'),
    ];
    $form['api']['api_user_lead_mapping'] = [
      '#title' => $this->t('Mapping of Drupal user fields to Mautic lead fields'),
      '#type' => 'textarea',
      '#default_value' => $config->get('api.user_lead_mapping'),
      '#description' => $this->t('Drupal field and Mautic field separated by pipe |. One field per line. <br>For example: <code>mail|email</code>'),
      '#maxlength' => 500,
      '#states' => [
        'visible' => [
          ':input[name="api_synchronize_user"]' => ['checked' => TRUE],
        ],
        // Note: Form required marker is not visible as title is invisible.
        'required' => [
          ':input[name="api_synchronize_user"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['visibility'] = $this->buildVisibilityInterface([], $form_state);
    $form['visibility']['#tree'] = TRUE;
    return parent::buildForm($form, $form_state);
  }

  /**
   * Helper function for building the visibility UI form.
   *
   * Originally from the `block` module.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form array with the visibility UI added in.
   */
  protected function buildVisibilityInterface(array $form, FormStateInterface $form_state) {
    $form['visibility_tabs'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Where to put the Mautic JS snippet?'),
      '#parents' => ['visibility_tabs'],
      '#attached' => [
        'library' => [
          'block/drupal.block',
        ],
      ],
    ];

    $config = $this->config('advanced_mautic_integration.settings');
    $visibility = $config->get('visibility');
    $definitions = $this->conditionManager->getFilteredDefinitions('mautic', $form_state->getTemporaryValue('gathered_contexts'));
    foreach ($definitions as $condition_id => $definition) {
      // Don't display the current theme condition.
      if ($condition_id == 'current_theme') {
        continue;
      }
      // Don't display the language condition until we have multiple languages.
      if ($condition_id == 'language' && !$this->languageManager->isMultilingual()) {
        continue;
      }

      /** @var \Drupal\Core\Condition\ConditionInterface $condition */
      $condition = $this->conditionManager->createInstance($condition_id, $visibility[$condition_id] ?? []);
      $form_state->set(['conditions', $condition_id], $condition);
      $condition_form = $condition->buildConfigurationForm([], $form_state);
      $condition_form['#type'] = 'details';
      $condition_form['#title'] = $condition->getPluginDefinition()['label'];
      $condition_form['#group'] = 'visibility_tabs';
      $form[$condition_id] = $condition_form;
    }

    if (isset($form['entity_bundle:node'])) {
      $form['entity_bundle:node']['negate']['#type'] = 'value';
      $form['entity_bundle:node']['negate']['#title_display'] = 'invisible';
      $form['entity_bundle:node']['negate']['#value'] = $form['entity_bundle:node']['negate']['#default_value'];
    }
    if (isset($form['user_role'])) {
      $form['user_role']['#title'] = $this->t('Roles');
      unset($form['user_role']['roles']['#description']);
      $form['user_role']['negate']['#type'] = 'value';
      $form['user_role']['negate']['#value'] = $form['user_role']['negate']['#default_value'];
    }
    if (isset($form['request_path'])) {
      $form['request_path']['#title'] = $this->t('Pages');
      $form['request_path']['negate']['#type'] = 'radios';
      $form['request_path']['negate']['#default_value'] = (int) $form['request_path']['negate']['#default_value'];
      $form['request_path']['negate']['#title_display'] = 'invisible';
      $form['request_path']['negate']['#options'] = [
        $this->t('Show for the listed pages'),
        $this->t('Hide for the listed pages'),
      ];
    }
    if (isset($form['language'])) {
      $form['language']['negate']['#type'] = 'value';
      $form['language']['negate']['#value'] = $form['language']['negate']['#default_value'];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $config = $this->config('advanced_mautic_integration.settings');

    // Disallow empty list of download file extensions.
    if ($form_state->getValue('track_files') && $form_state->isValueEmpty('track_files_extensions')) {
      $form_state->setErrorByName('track_files_extensions', $this->t('List of download file extensions cannot be empty.'));
    }

    // Disallow empty list of field mappings.
    if ($form_state->getValue('api_synchronize_user') && $form_state->isValueEmpty('api_user_lead_mapping')) {
      $form_state->setErrorByName('api_user_lead_mapping', $this->t('List of field mappings cannot be empty.'));
    }

    // Validate the format of "api_user_lead_mapping".
    $api_user_lead_mapping = $form_state->getValue('api_user_lead_mapping');
    if (!empty($api_user_lead_mapping)) {
      $lines = explode("\n", $api_user_lead_mapping);
      foreach ($lines as $line) {
        $values = array_map('trim', explode('|', $line));
        if (count($values) != 2 || empty($values[0]) || empty($values[1])) {
          $form_state->setErrorByName('api_user_lead_mapping', $this->t('Invalid format of field mappings.'));
          break;
        }
      }
    }

    // Validate the "track_default_parameters" as JSON.
    $track_default_parameters = $form_state->getValue('track_default_parameters');
    if (!empty($track_default_parameters)) {
      json_decode($track_default_parameters);
      if (json_last_error() !== JSON_ERROR_NONE) {
        $form_state->setErrorByName('track_default_parameters', $this->t('Invalid JSON format.'));
      }
    }

    // Validate URLs.
    $mautic_js_url = $form_state->getValue('track_url');
    $mautic_api_url = $form_state->getValue('api_url');

    // Trim URLs from slashes.
    $mautic_js_url = trim($mautic_js_url, '/');
    $form_state->setValue('track_url', $mautic_js_url);
    $mautic_api_url = trim($mautic_api_url, '/');
    $form_state->setValue('api_url', $mautic_api_url);

    // Validate the URL schema.
    if (!empty($mautic_js_url) && !UrlHelper::isValid($mautic_js_url, TRUE)) {
      $form_state->setErrorByName('track_url', $this->t('The URL is not valid.'));
    }
    if (!empty($mautic_api_url) && !UrlHelper::isValid($mautic_api_url, TRUE)) {
      $form_state->setErrorByName('api_url', $this->t('The URL is not valid.'));
    }

    // Validate the API credentials.
    if (!empty($mautic_api_url) && empty($form_state->getValue('api_user'))) {
      $form_state->setErrorByName('api_user', $this->t('The user is required.'));
    }
    if (!empty($mautic_api_url) && empty($form_state->getValue('api_password'))) {
      // Check if the password is empty in the config.
      $password = $config->get('api.password');
      if (empty($password)) {
        $form_state->setErrorByName('api_password', $this->t('The password is required.'));
      }
      else {
        // Keep the current password.
        $form_state->setValue('api_password', $password);
      }
    }

    $this->validateVisibility($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('advanced_mautic_integration.settings');
    $config
      ->set('track.url', $form_state->getValue('track_url'))
      ->set('track.pageviews', $form_state->getValue('track_pageviews'))
      ->set('track.outbound', $form_state->getValue('track_outbound'))
      ->set('track.mailto', $form_state->getValue('track_mailto'))
      ->set('track.tel', $form_state->getValue('track_tel'))
      ->set('track.files', $form_state->getValue('track_files'))
      ->set('track.files_extensions', $form_state->getValue('track_files_extensions'))
      ->set('track.default_parameters', $form_state->getValue('track_default_parameters'))
      ->set('api.url', $form_state->getValue('api_url'))
      ->set('api.user', $form_state->getValue('api_user'))
      ->set('api.password', $form_state->getValue('api_password'))
      ->set('api.synchronize_user', $form_state->getValue('api_synchronize_user'))
      ->set('api.user_lead_mapping', $form_state->getValue('api_user_lead_mapping'))
      ->save();

    $this->submitVisibility($form, $form_state);
  }

  /**
   * Helper function to independently validate the visibility UI.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function validateVisibility(array $form, FormStateInterface $form_state) {
    // Validate visibility condition settings.
    foreach ($form_state->getValue('visibility') as $condition_id => $values) {
      // Allow the condition to validate the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      assert($condition instanceof ConditionInterface);
      $condition->validateConfigurationForm($form['visibility'][$condition_id], SubformState::createForSubform($form['visibility'][$condition_id], $form, $form_state));
    }
  }

  /**
   * Helper function to independently submit the visibility UI.
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  protected function submitVisibility(array $form, FormStateInterface $form_state) {
    $visibility_collection = new ConditionPluginCollection($this->conditionManager, []);
    foreach ($form_state->getValue('visibility') as $condition_id => $values) {
      // Allow the condition to submit the form.
      $condition = $form_state->get(['conditions', $condition_id]);
      assert($condition instanceof ConditionInterface);
      $condition->submitConfigurationForm($form['visibility'][$condition_id], SubformState::createForSubform($form['visibility'][$condition_id], $form, $form_state));

      $condition_configuration = $condition->getConfiguration();
      $visibility_collection->addInstanceId($condition_id, $condition_configuration);
    }

    $this->config('advanced_mautic_integration.settings')
      ->set('visibility', $visibility_collection->getConfiguration())
      ->save();
  }

}
