<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Utility\Token;

/**
 * A helper class to generate JS script and data.
 */
final class MauticScript implements MauticScriptInterface {

  /**
   * Constructs a MauticScript object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly Token $token,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function getScript(BubbleableMetadata $bubbleable_metadata = NULL): string {
    $config = $this->configFactory->get('advanced_mautic_integration.settings');
    $bubbleable_metadata->addCacheableDependency($config);
    $tracking_url = $config->get('track.url');

    $script = "(function(w,d,t,u,n,a,m){w['MauticTrackingObject']=n;";
    $script .= "w[n]=w[n]||function(){(w[n].q=w[n].q||[]).push(arguments)},a=d.createElement(t),";
    $script .= "m=d.getElementsByTagName(t)[0];a.async=1;a.src=u;m.parentNode.insertBefore(a,m)";
    $script .= "})(window,document,'script','$tracking_url/mtc.js','mt');";

    return $script;
  }

  /**
   * {@inheritdoc}
   */
  public function getTrackingSettings(BubbleableMetadata $bubbleable_metadata = NULL): array {
    $config = $this->configFactory->get('advanced_mautic_integration.settings');
    $bubbleable_metadata->addCacheableDependency($config);

    $tracking_settings = [
      'trackUrl' => $config->get('track.url') ?? '',
      'trackPageviews' => $config->get('track.pageviews') ?? FALSE,
      'trackOutbound' => $config->get('track.outbound') ?? FALSE,
      'trackMailto' => $config->get('track.mailto') ?? FALSE,
      'trackTel' => $config->get('track.tel') ?? FALSE,
      'trackFiles' => $config->get('track.files') ?? FALSE,
      'trackFilesExtensions' => $config->get('track.files_extensions') ?? '',
      'trackDefaultParameters' => '{}',
    ];

    if ($track_default_parameters = $config->get('track.default_parameters')) {
      $track_default_parameters = $this->token->replacePlain($track_default_parameters, [], ['clear' => TRUE], $bubbleable_metadata);

      // Decode parameters from JSON and remove items with empty values.
      $parameter_array = json_decode($track_default_parameters, TRUE);
      $parameter_array = array_filter($parameter_array);

      $tracking_settings['trackDefaultParameters'] = $parameter_array;
    }

    return $tracking_settings;
  }

}
