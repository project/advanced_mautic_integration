<?php

declare(strict_types=1);

namespace Drupal\advanced_mautic_integration;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The class that synchronizes Drupal users with Mautic leads.
 */
final class UserSynchronizer implements UserSynchronizerInterface {

  /**
   * Constructs a UserSynchronizer object.
   */
  public function __construct(
    private readonly ConfigFactoryInterface $configFactory,
    private readonly MauticApiWrapperInterface $apiWrapper,
    private readonly AccountProxyInterface $currentUser,
    private readonly RequestStack $requestStack,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function push(UserInterface $user): void {
    // Identify the lead in Mautic.
    $lead_id = $this->getLeadIdForUser($user);

    // Perform the update in the API.
    if ($lead_data = $this->convertUserToLead($user)) {
      $api = $this->apiWrapper->getApi('contacts');
      if ($lead_id) {
        $api->edit((int) $lead_id, $lead_data);
      }
      else {
        $api->create($lead_data);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLeadIdForUser(UserInterface $user): ?string {
    // There's an edge case when we fetch the lead ID for the current user.
    // In this case, we need to use the Mautic ID from the cookie.
    if ($user->id() == $this->currentUser->id()) {
      $lead_id = $this->requestStack->getCurrentRequest()->cookies->get('mtc_id');
    }

    if (empty($lead_id)) {
      $lead = $this->getLeadByParameter($user->getEmail());
      $lead_id = $lead['id'] ?? NULL;
    }

    return (string) $lead_id;
  }

  /**
   * {@inheritdoc}
   */
  public function convertUserToLead(UserInterface $user): array {
    $config = $this->configFactory->get('advanced_mautic_integration.settings');
    $mappings = explode("\n", $config->get('api.user_lead_mapping'));
    $lead_data = [];
    foreach ($mappings as $field_pair) {
      [$drupal_field, $mautic_field] = explode('|', $field_pair);
      if ($user->hasField($drupal_field)) {
        $value = $user->get($drupal_field)->value;
        $lead_data[$mautic_field] = $value;
      }
    }
    return $lead_data;
  }

  /**
   * {@inheritdoc}
   */
  public function getLeadByParameter($parameter): ?array {
    $api = $this->apiWrapper->getApi('contacts');
    $contacts_result = $api->getList($parameter);

    if (!empty($contacts_result['contacts'])) {
      return reset($contacts_result['contacts']);
    }
    else {
      return NULL;
    }
  }

}
